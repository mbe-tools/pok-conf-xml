
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];
SAMPLING_PORT_ID_TYPE part1_p1_port_id;

/******************************************************************************/
/*                                    MAIN                                    */

void process1();

int main ()
{
  RETURN_CODE_TYPE ret;
  
  CREATE_SAMPLING_PORT ("part1_p1",
    sizeof(int), SOURCE, 10,
      &(part1_p1_port_id), &(ret));
  
  PROCESS_ATTRIBUTE_TYPE tattr;    
  tattr.ENTRY_POINT = process1;
  tattr.PERIOD = 1000;
  tattr.DEADLINE = 1000;
  tattr.TIME_CAPACITY = 1;
  tattr.BASE_PRIORITY = 5;
  strcpy(tattr.NAME, "thread1");
  CREATE_PROCESS (&(tattr), &(arinc_threads[1]), &(ret));
  START (arinc_threads[1], &(ret));
  SET_PARTITION_MODE (NORMAL, &(ret));
  return (0);
}

void process1()
{
  RETURN_CODE_TYPE ret;
  int i=0;
  while(1)
  {
    i++;
    printf("partition part1 sends value through port p1: %d\n", i);
    WRITE_SAMPLING_MESSAGE (part1_p1_port_id, 
    						(MESSAGE_ADDR_TYPE) &i,
    						sizeof(int), 
    						&ret);
    PERIODIC_WAIT (&ret);
  }
}
