
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];
SAMPLING_PORT_ID_TYPE part3_p3_port_id, part3_p4_port_id;

/******************************************************************************/
/*                                    MAIN                                    */

void process1();

int main ()
{
  RETURN_CODE_TYPE ret;
  
  CREATE_SAMPLING_PORT ("part3_p3",
    sizeof(int), DESTINATION, 10,
      &(part3_p3_port_id), &(ret));
  
  CREATE_SAMPLING_PORT ("part3_p4",
    sizeof(int), DESTINATION, 10,
      &(part3_p4_port_id), &(ret));
  
  PROCESS_ATTRIBUTE_TYPE tattr;    
  tattr.ENTRY_POINT = process1;
  tattr.PERIOD = 1000;
  tattr.DEADLINE = 1000;
  tattr.TIME_CAPACITY = 1;
  tattr.BASE_PRIORITY = 5;
  strcpy(tattr.NAME, "thread1");
  CREATE_PROCESS (&(tattr), &(arinc_threads[1]), &(ret));
  START (arinc_threads[1], &(ret));
  SET_PARTITION_MODE (NORMAL, &(ret));
  return (0);
}

void process1()
{
  RETURN_CODE_TYPE ret;
  VALIDITY_TYPE message_validity;
  MESSAGE_SIZE_TYPE message_size;
  int i=0;
  int j=0;
  while(1)
  {
  	
  	READ_SAMPLING_MESSAGE (part3_p3_port_id,
  							(MESSAGE_ADDR_TYPE) &i,
  							&message_size, 
  							&message_validity, 
  							&ret);
    printf("partition part3 received value through port p3: %d\n", i);
    READ_SAMPLING_MESSAGE (part3_p4_port_id, 
    						(MESSAGE_ADDR_TYPE) &j, 
    						&message_size, 
    						&message_validity, 
    						&ret);
    printf("partition part3 received value through port p4: %d\n", j);
    PERIODIC_WAIT (&ret);
  }
}
