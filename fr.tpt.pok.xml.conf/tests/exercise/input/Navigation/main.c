
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];
SAMPLING_PORT_ID_TYPE Navigation_toDisp_port_id;
SAMPLING_PORT_ID_TYPE Navigation_fromRad_port_id;

/******************************************************************************/
/*                                    MAIN                                    */

void Navigation_process_50();
void Navigation_process_300();

int main ()
{
  RETURN_CODE_TYPE ret;
  
  // To be completed (exercise 2): create ports

  // To be completed (exercise 1): create and start processes

  
  SET_PARTITION_MODE (NORMAL, &(ret));
  return (0);
}

void Navigation_process_50()
{
  RETURN_CODE_TYPE ret;
  VALIDITY_TYPE message_validity;
  MESSAGE_SIZE_TYPE message_size;
  int i=0;
  while(1)
  {
    printf("Partition Navigation activation (50ms)\n");
    // To be completed (exercise 2): receive value from Radar
    printf("partition Navigation received value through port fromRad: %d\n", i);
    // To be completed (exercise 2): send value to Display
    PERIODIC_WAIT (&ret);
  }
}

void Navigation_process_300()
{
  RETURN_CODE_TYPE ret;
  while(1)
  {
    printf("Partition Navigation activation (300ms)\n");
    PERIODIC_WAIT (&ret);
  }
}
