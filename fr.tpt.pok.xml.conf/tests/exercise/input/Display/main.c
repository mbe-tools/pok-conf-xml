
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];
SAMPLING_PORT_ID_TYPE part3_fromNav_port_id;

/******************************************************************************/
/*                                    MAIN                                    */

void Display_process_100();
void Display_process_300();

int main ()
{
  RETURN_CODE_TYPE ret;
  
  // To be completed (exercise 2): create ports
  
  PROCESS_ATTRIBUTE_TYPE tattr;    
  tattr.ENTRY_POINT = Display_process_100;
  tattr.PERIOD = 100;
  tattr.DEADLINE = 100;
  tattr.TIME_CAPACITY = 1;
  tattr.BASE_PRIORITY = 10;
  strcpy(tattr.NAME, "thread_100");
  CREATE_PROCESS (&(tattr), &(arinc_threads[1]), &(ret));
  
  tattr.ENTRY_POINT = Display_process_300;
  tattr.PERIOD = 300;
  tattr.DEADLINE = 300;
  tattr.TIME_CAPACITY = 1;
  tattr.BASE_PRIORITY = 5;
  strcpy(tattr.NAME, "thread_300");
  CREATE_PROCESS (&(tattr), &(arinc_threads[2]), &(ret));
  
  START (arinc_threads[1], &(ret));
  START (arinc_threads[2], &(ret));
  SET_PARTITION_MODE (NORMAL, &(ret));
  
  return (0);
}

void Display_process_100()
{
  RETURN_CODE_TYPE ret;
  VALIDITY_TYPE message_validity;
  MESSAGE_SIZE_TYPE message_size;
  int i=0;
  while(1)
  {
  	printf("Partition Display activation (100ms)\n");
  	// To be completed (exercise 2): receive value from Navigation
    printf("partition Display received value through port fromNav: %d\n", i);
    PERIODIC_WAIT (&ret);
  }
}

void Display_process_300()
{
  RETURN_CODE_TYPE ret;
  while(1)
  {
    printf("Partition Display activation (300ms)\n");
    PERIODIC_WAIT (&ret);
  }
}
