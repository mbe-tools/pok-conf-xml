
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];
SAMPLING_PORT_ID_TYPE Radar_toNav_port_id;

/******************************************************************************/
/*                                    MAIN                                    */

void Radar_process_100();

int main ()
{
  RETURN_CODE_TYPE ret;

  // To be completed (exercise 2): create ports

  // To be completed (exercise 1): create and start processes

  SET_PARTITION_MODE (NORMAL, &(ret));
  return (0);
}

void Radar_process_100()
{
  RETURN_CODE_TYPE ret;
  int i=0;
  while(1)
  {
    printf("Partition Radar activation (100ms)\n");
    i++;
    printf("partition Radar sends value through port toNav: %d\n", i);
	// To be completed (exercise 2): send value to Navigation
    PERIODIC_WAIT (&ret);
  }
}
