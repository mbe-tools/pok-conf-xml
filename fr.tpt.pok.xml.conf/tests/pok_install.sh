sudo apt-get update
sudo apt-get install binutils
sudo apt-get install qemu
sudo apt-get install libxml-libxml-perl
sudo apt-get install openjdk-8-jre-headless
mkdir ~/opt
cd ~/opt
wget http://perso.telecom-paristech.fr/~borde/arinc653/pok.tar.gz
tar -zxf pok.tar.gz
rm -f pok.tar.gz
cd –
cd ~/opt/pok
wget http://perso.telecom-paristech.fr/~borde/arinc653/pok-conf.jar
wget http://perso.telecom-paristech.fr/~borde/arinc653/pok-conf
chmod o+x pok-conf
./misc/conf-env.pl
cd -
export POK_PATH= ~/opt/pok
export PATH=$POK_PATH:$PATH
