
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];
SAMPLING_PORT_ID_TYPE Radar_toNav_port_id;

/******************************************************************************/
/*                                    MAIN                                    */

void Radar_process_100();

int main ()
{
  RETURN_CODE_TYPE ret;
  
  CREATE_SAMPLING_PORT ("Radar_toNav",
    sizeof(int), SOURCE, 10,
      &(Radar_toNav_port_id), &(ret));
  
  PROCESS_ATTRIBUTE_TYPE tattr;    
  tattr.ENTRY_POINT = Radar_process_100;
  tattr.PERIOD = 100;
  tattr.DEADLINE = 100;
  tattr.TIME_CAPACITY = 1;
  tattr.BASE_PRIORITY = 5;
  strcpy(tattr.NAME, "thread1");
  CREATE_PROCESS (&(tattr), &(arinc_threads[1]), &(ret));
  START (arinc_threads[1], &(ret));
  SET_PARTITION_MODE (NORMAL, &(ret));
  return (0);
}

void Radar_process_100()
{
  RETURN_CODE_TYPE ret;
  int i=0;
  while(1)
  {
    printf("Partition Radar activation (100ms)\n");
    i++;
    printf("partition Radar sends value through port toNav: %d\n", i);
    WRITE_SAMPLING_MESSAGE (Radar_toNav_port_id, 
    						(MESSAGE_ADDR_TYPE) &i,
    						sizeof(int), 
    						&ret);
    PERIODIC_WAIT (&ret);
  }
}
