
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];
SAMPLING_PORT_ID_TYPE Navigation_toDisp_port_id;
SAMPLING_PORT_ID_TYPE Navigation_fromRad_port_id;

/******************************************************************************/
/*                                    MAIN                                    */

void Navigation_process_50();
void Navigation_process_300();

int main ()
{
  RETURN_CODE_TYPE ret;
  
  CREATE_SAMPLING_PORT ("Navigation_toDisp",
    sizeof(int), SOURCE, 10,
      &(Navigation_toDisp_port_id), &(ret));
  
  CREATE_SAMPLING_PORT ("Navigation_fromRad",
    sizeof(int), DESTINATION, 10,
      &(Navigation_fromRad_port_id), &(ret));
  
  PROCESS_ATTRIBUTE_TYPE tattr_300;
  tattr_300.ENTRY_POINT = Navigation_process_300;
  tattr_300.PERIOD = 300;
  tattr_300.DEADLINE = 300;
  tattr_300.TIME_CAPACITY = 1;
  tattr_300.BASE_PRIORITY = 5;
  strcpy(tattr_300.NAME, "thread_300");
  CREATE_PROCESS (&(tattr_300), &(arinc_threads[1]), &(ret));
  START (arinc_threads[1], &(ret));
  
  PROCESS_ATTRIBUTE_TYPE tattr;
  tattr.ENTRY_POINT = Navigation_process_50;
  tattr.PERIOD = 50;
  tattr.DEADLINE = 50;
  tattr.TIME_CAPACITY = 1;
  tattr.BASE_PRIORITY = 10;
  strcpy(tattr.NAME, "thread_50");
  CREATE_PROCESS (&(tattr), &(arinc_threads[2]), &(ret));
  START (arinc_threads[2], &(ret));
  
  SET_PARTITION_MODE (NORMAL, &(ret));
  return (0);
}

void Navigation_process_50()
{
  RETURN_CODE_TYPE ret;
  VALIDITY_TYPE message_validity;
  MESSAGE_SIZE_TYPE message_size;
  int i=0;
  while(1)
  {
    printf("Partition Navigation activation (50ms)\n");
    READ_SAMPLING_MESSAGE (Navigation_fromRad_port_id, (MESSAGE_ADDR_TYPE) &i, &message_size, &message_validity, &ret);
    printf("partition Navigation received value through port fromRad: %d\n", i);
    WRITE_SAMPLING_MESSAGE (Navigation_toDisp_port_id, (MESSAGE_ADDR_TYPE) &i, sizeof(int), &ret);
    PERIODIC_WAIT (&ret);
  }
}

void Navigation_process_300()
{
  RETURN_CODE_TYPE ret;
  while(1)
  {
    printf("Partition Navigation activation (300ms)\n");
    PERIODIC_WAIT (&ret);
  }
}
