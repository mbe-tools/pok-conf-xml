
/******************************************************************************/
/*                                 INCLUSION                                  */

#include "main.h"
#include <libc/stdio.h>
#include <libc/string.h>

/******************************************************************************/
/*                              GLOBAL VARIABLES                              */

PROCESS_ID_TYPE arinc_threads[POK_CONFIG_NB_THREADS];

/******************************************************************************/
/*                                    MAIN                                    */

void Logging_process_300();

int main ()
{
  RETURN_CODE_TYPE ret;
    
  PROCESS_ATTRIBUTE_TYPE tattr;    
  tattr.ENTRY_POINT = Logging_process_300;
  tattr.PERIOD = 300;
  tattr.DEADLINE = 300;
  tattr.TIME_CAPACITY = 1;
  tattr.BASE_PRIORITY = 5;
  
  strcpy(tattr.NAME, "thread1");
  CREATE_PROCESS (&(tattr), &(arinc_threads[1]), &(ret));
  
  START (arinc_threads[1], &(ret));
  SET_PARTITION_MODE (NORMAL, &(ret));
  return (0);
}

void Logging_process_300()
{
  RETURN_CODE_TYPE ret;
  while(1)
  {
    printf("partition Logging activated (300ms)\n");
    PERIODIC_WAIT (&ret);
  }
}
