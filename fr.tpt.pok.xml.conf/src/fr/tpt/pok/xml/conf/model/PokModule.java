package fr.tpt.pok.xml.conf.model;

import java.util.ArrayList;
import java.util.List;

public class PokModule {

	private PokModule(){};
	
	private static PokModule pokSystemSingleton;
	
	public static PokModule get()
	{
		if(pokSystemSingleton == null)
			pokSystemSingleton = new PokModule();
		return pokSystemSingleton;
	}
	
	private List<PokPartition> _partitionList =
			new ArrayList<PokPartition>();
	
	private List<PokPartitionWindow> _partitionWindowList =
			new ArrayList<PokPartitionWindow>();
	
	private List<PokChannel> _channelList =
			new ArrayList<PokChannel>();
	
	private int _majorFrame = 0;
	
	private int _minorFrame = 0;
	
	private FlushType _flushType = FlushType.MAF;
	
	public enum FlushType
	{
		MAF,
		MIF,
		WINDOW
	};
	
	public List<PokPartition> get_partitionList() {
		return _partitionList;
	}
	
	public List<PokPartitionWindow> get_partitionWindowList() {
		return _partitionWindowList;
	}

	public int get_majorFrame() {
		return _majorFrame;
	}

	public void set_majorFrame(int _majorFrame) {
		this._majorFrame = _majorFrame;
	}

	public int get_minorFrame() {
		return _minorFrame;
	}

	public void set_minorFrame(int _minorFrame) {
		this._minorFrame = _minorFrame;
	}

	public List<PokChannel> get_channelList() {
		return _channelList;
	}

	public FlushType get_flushType()
	{
		return _flushType;
	}
	
	public void set_flushType(String flushTypeString) {
		if(flushTypeString.equalsIgnoreCase("window"))
			_flushType = FlushType.WINDOW;
		else if(flushTypeString.equalsIgnoreCase("minor_frame"))
		{
			_flushType = FlushType.MIF;
			if(_minorFrame<=0)
			{
				System.err.println("ERROR: flush type is minor_frame but "
						+ "the minor_frame value is undefined");
			}
		}
	}
	
}
