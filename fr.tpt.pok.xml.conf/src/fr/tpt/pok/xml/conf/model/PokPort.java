package fr.tpt.pok.xml.conf.model;

import fr.tpt.pok.xml.conf.utils.PokConfUtils;

public class PokPort {

	private String _name;
	private PokPartition _parent;
	private PortKind _kind;
	private PortDirection _direction;
	
	public PokPort(String name, 
			PokPartition parent, 
			String kind,
			String direction)
	{
		set_name(name);
		_parent = parent;
		set_kind(kind);
		set_direction(direction);
	}
	
	private void set_direction(String direction) {
		if(direction.equalsIgnoreCase("in"))
			_direction = PortDirection.IN;
		else if(direction.equalsIgnoreCase("out"))
			_direction = PortDirection.OUT;
		else
		{
			System.err.println("ERROR: port direction should be either in or out");
			System.exit(-1);
		}
	}

	public PortDirection get_direction()
	{
		return _direction;
	}
	
	private void set_kind(String kind) {
		if(kind.equalsIgnoreCase("sampling"))
			_kind = PortKind.SAMPLING;
		else if(kind.equalsIgnoreCase("queueing"))
			_kind = PortKind.QUEUEING;
		else
		{
			System.err.println("ERROR: port kind should be either sampling or queueing");
			System.exit(-1);
		}
	}

	public PortKind get_kind() {
		return _kind;
	}
	
	public String get_name() {
		return _name;
	}
	
	public String get_C_LocalIdentifier()
	{
		return _parent.get_name()+"_"+get_name();
	}
	
	public String get_C_GlobalIdentifier()
	{
		return _parent.get_name()+"_"+get_name()+"_global";
	}
	
	private void set_name(String _name) {
		if(PokConfUtils.containsSpecialCharacter(_name))
		{
			System.err.println("Partition name" + _name + "contains a special character");
			System.exit(-1);
		}
		this._name = PokConfUtils.removeSpaces(_name);
	}

	public PokPartition get_parent() {
		return _parent;
	}

	public boolean isQueueingPort() {
		return _kind.equals(PortKind.QUEUEING);
	}

	public boolean isSamplingPort() {
		return _kind.equals(PortKind.SAMPLING);
	}

	public enum PortKind
	{
		SAMPLING,
		QUEUEING
	}
	
	public enum PortDirection
	{
		IN,
		OUT
	}
	
}
