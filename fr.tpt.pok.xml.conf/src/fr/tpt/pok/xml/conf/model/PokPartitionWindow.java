package fr.tpt.pok.xml.conf.model;

public class PokPartitionWindow {

	private PokPartition _partition;
	private int _duration;
	
	public PokPartitionWindow(PokPartition partition, int duration)
	{
		_partition = partition;
		_duration = duration;
	}
	
	public PokPartition get_partition() {
		return _partition;
	}
	
	public int get_duration() {
		return _duration;
	}	
}
