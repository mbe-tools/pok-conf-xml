package fr.tpt.pok.xml.conf.model;

import java.util.ArrayList;
import java.util.List;

import fr.tpt.pok.xml.conf.utils.PokConfUtils;

public class PokPartition {

	private String _name;
	private int _partitionSize;
	private int _numberOfProcesses;
	
	private List<PokPort> _patritionPortList = 
			new ArrayList<PokPort>();
	
	public PokPartition(String name,
			int partitionSize,
			int numberOfProcesses)
	{
		set_name(name);
		_partitionSize = partitionSize;
		_numberOfProcesses = numberOfProcesses;
	}
	
	public int get_partitionSize() {
		return _partitionSize;
	}
	
	public int get_numberOfProcesses() {
		return _numberOfProcesses;
	}
	
	
	public List<PokPort> get_patritionPortList() {
		return _patritionPortList;
	}
	
	public void add_PartitionPort(PokPort port) {
		this._patritionPortList.add(port);
	}
	
	public String get_name() {
		return _name;
	}
	
	private void set_name(String _name) {
		if(PokConfUtils.containsSpecialCharacter(_name))
		{
			System.err.println("Partition name" + _name + "contains a special character");
			System.exit(-1);
		}
		this._name = PokConfUtils.removeSpaces(_name);
	}
	
}
