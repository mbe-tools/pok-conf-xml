package fr.tpt.pok.xml.conf.model;

public class PokChannel {
	
	private PokPort _source;
	private PokPort _destination;
	
	public PokChannel(PokPort source,
			PokPort destination)
	{
		_source = source;
		_destination = destination;
	}
	
	public PokPort get_source() {
		return _source;
	}
	
	
	public PokPort get_destination() {
		return _destination;
	}
	
	
	
}
