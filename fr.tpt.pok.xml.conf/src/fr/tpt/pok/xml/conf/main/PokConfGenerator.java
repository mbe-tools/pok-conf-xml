package fr.tpt.pok.xml.conf.main;

import java.io.File;

import fr.tpt.pok.xml.conf.backend.PokMainConfigurationGenerator;
import fr.tpt.pok.xml.conf.parser.PokConfParser;

public class PokConfGenerator {

	public static void main(String[] args) {
		
		if(args.length != 2)
		{
			System.err.println("Wrong number of arguments: invoke "
					+ "pok-conf xml-file output-dir");
			System.exit(-1);
		}
		
		// Parse XML file
		File xmlConfFile = new File(args[0]);
		if(false == xmlConfFile.exists())
		{
			System.err.println("XML configuration file does not exist");
			System.exit(-1);
		}
		
		PokConfParser parser = new PokConfParser();  
		parser.parseConfigurationFile(xmlConfFile.getAbsolutePath());
		
		// Generate configuration source files
		PokMainConfigurationGenerator gen = 
				new PokMainConfigurationGenerator();
		
		File outputDir = new File(args[1]);
		if(false == outputDir.exists())
		{
			System.err.println("Output dir does not exist");
			System.exit(-1);
		}
		
		gen.generateFiles(outputDir.getAbsolutePath());
		
	}

}
