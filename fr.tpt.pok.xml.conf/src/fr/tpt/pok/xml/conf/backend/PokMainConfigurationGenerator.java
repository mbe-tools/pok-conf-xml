package fr.tpt.pok.xml.conf.backend;

public class PokMainConfigurationGenerator {

	
	PokKernelConfigurationGenerator _kernelGenerator =
			new PokKernelConfigurationGenerator();
	
	PokPartitionConfigurationGenerator _partitionGenerator =
			new PokPartitionConfigurationGenerator();
	
	
	public void generateFiles(String outputDirPath)
	{
		_kernelGenerator.generateFiles(outputDirPath);
		_partitionGenerator.generateFiles(outputDirPath);
	}
}
