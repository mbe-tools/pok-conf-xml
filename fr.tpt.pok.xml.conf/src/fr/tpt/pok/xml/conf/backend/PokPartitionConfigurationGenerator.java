package fr.tpt.pok.xml.conf.backend;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import fr.tpt.pok.xml.conf.model.PokModule;
import fr.tpt.pok.xml.conf.model.PokPartition;
import fr.tpt.pok.xml.conf.model.PokPort;
import fr.tpt.pok.xml.conf.model.PokPort.PortKind;

class PokPartitionConfigurationGenerator {

	protected String _main_h_textToProduce="";
	
	
	public void generateFiles(String outputDirPath)
	{
		PokModule module = PokModule.get();
		for(PokPartition partition:module.get_partitionList())
		{
			_main_h_textToProduce="";
			fillInMain_H_String(partition);
			try
			{
				stringsToFiles(outputDirPath, partition);
			}
			catch(FileNotFoundException e)
			{
				System.err.println("Could not produce output file (FileNotFoundException)");
				e.printStackTrace();
				System.exit(-1);
			}
		}
		
		
		
	}

	private void fillInMain_H_String(PokPartition partition)
	{
		_main_h_textToProduce+="#ifndef __GENERATED_MAIN_H__\n";
		_main_h_textToProduce+="#define __GENERATED_MAIN_H__\n";
		_main_h_textToProduce+="\n";
		_main_h_textToProduce+="#define POK_GENERATED_CODE 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_CONSOLE 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_LIBC_STDIO 1\n";
		int nbOfThreads = partition.get_numberOfProcesses()+1; 
		_main_h_textToProduce+="#define POK_CONFIG_NB_THREADS "+
				nbOfThreads
				+"\n";
		boolean needsSampling=false;
		for(PokPort port:partition.get_patritionPortList())
			if(port.get_kind().equals(PortKind.SAMPLING))
			{
				_main_h_textToProduce+="#define POK_NEEDS_ARINC653_SAMPLING 1\n";
				needsSampling = true;
				break;
			}
		boolean needsQueueing=false;
		for(PokPort port:partition.get_patritionPortList())
			if(port.get_kind().equals(PortKind.QUEUEING))
			{
				_main_h_textToProduce+="#define POK_NEEDS_ARINC653_QUEUEING 1\n";
				needsQueueing = true;
				break;
			}
		
		_main_h_textToProduce+="#define POK_NEEDS_LIBC_STRING 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_PARTITIONS 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_PROTOCOLS 1\n";
		_main_h_textToProduce+="#define POK_CONFIG_STACKS_SIZE 20000\n";
		_main_h_textToProduce+="#define POK_NEEDS_ARINC653_TIME 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_ARINC653_PROCESS 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_ARINC653_PARTITION 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_MIDDLEWARE 1\n";
		_main_h_textToProduce+="#define POK_CONFIG_NEEDS_FUNC_UDIVDI3 1\n";
		_main_h_textToProduce+="#define POK_CONFIG_NEEDS_FUNC_UMODDI3 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_THREADS 1\n";
		_main_h_textToProduce+="#define POK_NEEDS_FUNCTION_TIME_GET 1\n";
		_main_h_textToProduce+="#include <arinc653/types.h>\n";
		_main_h_textToProduce+="#include <arinc653/process.h>\n";
		_main_h_textToProduce+="#include <arinc653/partition.h>\n";
		_main_h_textToProduce+="#include <arinc653/time.h>\n";
		
		if(needsSampling)
			_main_h_textToProduce+="#include \"arinc653/sampling.h\"\n";
		if(needsQueueing)
			_main_h_textToProduce+="#include \"arinc653/queueing.h\"\n";
		_main_h_textToProduce+="#include \"arinc653/types.h\"\n";
		
		_main_h_textToProduce+="\n";
		_main_h_textToProduce+="#endif\n";
		
	}
	
	private void stringsToFiles(String outputDirPath, PokPartition partition) 
			throws FileNotFoundException
	{
		
		String partitionDirName = partition.get_name();
		if(outputDirPath.endsWith("/"))
			outputDirPath+=partitionDirName+"/";
		else
			outputDirPath+="/"+partitionDirName+"/";
		
		File outputDir = new File(outputDirPath);
		if(!outputDir.exists())
			outputDir.mkdir();
		
		PrintWriter deployment_h_printer = new PrintWriter(outputDirPath+"main.h");
		deployment_h_printer.println(_main_h_textToProduce);
		deployment_h_printer.close();
		
	}
}
