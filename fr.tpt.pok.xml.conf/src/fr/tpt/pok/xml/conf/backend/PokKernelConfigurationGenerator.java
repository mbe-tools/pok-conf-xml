package fr.tpt.pok.xml.conf.backend;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import fr.tpt.pok.xml.conf.model.PokChannel;
import fr.tpt.pok.xml.conf.model.PokModule.FlushType;
import fr.tpt.pok.xml.conf.model.PokPartition;
import fr.tpt.pok.xml.conf.model.PokModule;
import fr.tpt.pok.xml.conf.model.PokPartitionWindow;
import fr.tpt.pok.xml.conf.model.PokPort;
import fr.tpt.pok.xml.conf.model.PokPort.PortDirection;
import fr.tpt.pok.xml.conf.model.PokPort.PortKind;
import fr.tpt.pok.xml.conf.utils.PokConfUtils;

class PokKernelConfigurationGenerator {

	protected String _deployment_c_textToProduce="";
	
	protected String _deployment_h_textToProduce="";
	
	protected String _routing_c_textToProduce="";
	
	protected String _routing_h_textToProduce="";
	
	protected String _makefile_textToProduce="";
	
	public void generateFiles(String outputDirPath)
	{
		
		List<String> reusedNames = 
				PokConfUtils.partitionsWithSameNames();
		if(false == reusedNames.isEmpty())
		{
			System.err.println("The same names "
					+ reusedNames
					+"are used for several partitions in "
					+ "the XML configuration file");
			System.exit(-1);
		}
		
		fillInDeployment_H_String();
		fillInDeployment_C_String();
		fillInRouting_H_String();
		fillInRouting_C_String();
		fillInMakefile_String();
		try
		{
			stringsToFiles(outputDirPath);
		}
		catch(FileNotFoundException e)
		{
			System.err.println("Could not produce output file (FileNotFoundException)");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void fillInMakefile_String() {
		_makefile_textToProduce += "export DEPLOYMENT_HEADER=$(shell pwd)/deployment.h\n";
		_makefile_textToProduce += "include $(POK_PATH)/misc/mk/config.mk\n";
		_makefile_textToProduce += "LO_TARGET = kernel.lo\n";
		_makefile_textToProduce += "LO_OBJS = deployment.o routing.o\n";
		_makefile_textToProduce += "LO_DEPS = pok.lo\n";
		_makefile_textToProduce += "all: clean kernel copy-kernel $(LO_TARGET)\n";
		_makefile_textToProduce += "clean: common-clean\n";
		_makefile_textToProduce += "include $(POK_PATH)/misc/mk/common-$(ARCH).mk\n";
		_makefile_textToProduce += "include $(POK_PATH)/misc/mk/rules-common.mk\n";
		_makefile_textToProduce += "include $(POK_PATH)/misc/mk/rules-kernel.mk\n";
	}

	private void fillInRouting_C_String() {
		
		_routing_c_textToProduce += "#include \"routing.h\"\n";
		_routing_c_textToProduce += "#include \"middleware/port.h\"\n";
		_routing_c_textToProduce += "#include <types.h>\n";
		
		PokModule module = PokModule.get();
		List<PokChannel> channels = module.get_channelList();
		if(channels.size()==0)
			return;
		
		List<PokPort> allPortsList = new ArrayList<PokPort>();
		
		for(PokPartition partition: module.get_partitionList())
		{
			List<PokPort> portList = partition.get_patritionPortList();
			allPortsList.addAll(portList);
			if(partition.get_patritionPortList().size()==0)
				continue;
			_routing_c_textToProduce += "uint8_t "+
				partition.get_name()+ "_partport["+portList.size()+"] = {";
			_routing_c_textToProduce += portList.get(0).get_C_LocalIdentifier();
			for(int i = 1;i<portList.size();i++)
			{
				_routing_c_textToProduce += ","+
						portList.get(i).get_C_LocalIdentifier();
			}
			_routing_c_textToProduce += "};\n";
			
			for(PokPort port : portList)
			{
				if(port.get_direction().equals(PortDirection.OUT))
				{
					List<PokPort> destinations = getPortDestinations(port);
					_routing_c_textToProduce += "uint8_t "+
							port.get_C_LocalIdentifier()+
							"_deployment_destinations["+destinations.size()+"] = {";
					
					_routing_c_textToProduce += destinations.get(0).get_C_GlobalIdentifier();
					for(int i = 1;i<destinations.size();i++)
					{
						_routing_c_textToProduce += ","+
								destinations.get(i).get_C_GlobalIdentifier();
					}
					_routing_c_textToProduce += "};\n";
				}
			}
		}
		
		_routing_c_textToProduce += "uint8_t "
				+ "pok_global_ports_to_local_ports[POK_CONFIG_NB_GLOBAL_PORTS] = {";
		_routing_c_textToProduce += allPortsList.get(0).get_C_LocalIdentifier();
		for(int i = 1;i<allPortsList.size();i++)
		{
			_routing_c_textToProduce += ","+
					allPortsList.get(i).get_C_LocalIdentifier();
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "uint8_t "
				+ "pok_global_ports_to_bus[POK_CONFIG_NB_GLOBAL_PORTS] = {";
		_routing_c_textToProduce += "invalid_bus";
		for(int i = 1;i<allPortsList.size();i++)
		{
			_routing_c_textToProduce += ","+
					"invalid_bus";
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "uint8_t "
				+ "pok_ports_nodes[POK_CONFIG_NB_GLOBAL_PORTS] = {";
		_routing_c_textToProduce += "the_cpu";
		for(int i = 1;i<allPortsList.size();i++)
		{
			_routing_c_textToProduce += ","+
					"the_cpu";
		}
		_routing_c_textToProduce += "};\n";
				
		_routing_c_textToProduce += "uint8_t "
				+ "pok_local_ports_to_global_ports[POK_CONFIG_NB_PORTS] = {";
		_routing_c_textToProduce += allPortsList.get(0).get_C_GlobalIdentifier();
		for(int i = 1;i<allPortsList.size();i++)
		{
			_routing_c_textToProduce += ","+
					allPortsList.get(i).get_C_GlobalIdentifier();
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "uint8_t "
				+ "pok_ports_nb_ports_by_partition[POK_CONFIG_NB_PARTITIONS] = {";
		for(PokPartition partition: module.get_partitionList())
		{
			_routing_c_textToProduce += partition.get_patritionPortList().size();
			_routing_c_textToProduce += ",";
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "uint8_t* "
				+ "pok_ports_by_partition[POK_CONFIG_NB_PARTITIONS] = {";
		for(PokPartition partition: module.get_partitionList())
		{
			if(partition.get_patritionPortList().size()==0)
			{
				_routing_c_textToProduce += "NULL,";
				continue;
			}
			_routing_c_textToProduce += partition.get_name()+"_partport";
			_routing_c_textToProduce += ",";
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "char* pok_ports_names[POK_CONFIG_NB_PORTS] = {";
		_routing_c_textToProduce += "\""+allPortsList.get(0).get_C_LocalIdentifier()+"\"";
		for(int i = 1;i<allPortsList.size();i++)
		{
			_routing_c_textToProduce += ","+
					"\""+allPortsList.get(i).get_C_LocalIdentifier()+"\"";
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "uint8_t pok_ports_identifiers[POK_CONFIG_NB_PORTS] = {";
		_routing_c_textToProduce += allPortsList.get(0).get_C_LocalIdentifier();
		for(int i = 1;i<allPortsList.size();i++)
		{
			_routing_c_textToProduce += ","+
					allPortsList.get(i).get_C_LocalIdentifier();
		}
		_routing_c_textToProduce += "};\n";
		
		
		_routing_c_textToProduce += "uint8_t pok_ports_nb_destinations[POK_CONFIG_NB_PORTS] = {";
		_routing_c_textToProduce += getPortDestinations(allPortsList.get(0)).size();
		for(int i = 1;i<allPortsList.size();i++)
		{
			_routing_c_textToProduce += ","+
					getPortDestinations(allPortsList.get(i)).size();
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "uint8_t* pok_ports_destinations[POK_CONFIG_NB_PORTS] = {";
		for(PokPort port: allPortsList)
		{
			List<PokPort> destinations = getPortDestinations(port);
			if(destinations.size()>0)
				_routing_c_textToProduce += port.get_C_LocalIdentifier()+"_deployment_destinations,";
			else
				_routing_c_textToProduce += "NULL,";
		}
		_routing_c_textToProduce += "};\n";
		
		_routing_c_textToProduce += "uint8_t pok_ports_kind[POK_CONFIG_NB_PORTS] = {";
		for(PokPort port: allPortsList)
		{
			if(port.get_kind().equals(PortKind.SAMPLING))
				_routing_c_textToProduce += "POK_PORT_KIND_SAMPLING,";
			else
				_routing_c_textToProduce += "POK_PORT_KIND_QUEUEING,";
		}
		_routing_c_textToProduce += "};\n";
	}

	private List<PokPort> getPortDestinations(PokPort port) {
		List<PokPort> destinations = new ArrayList<PokPort>(); 
		PokModule module = PokModule.get();
		for(PokChannel channel : module.get_channelList())
		{
			if(channel.get_source().equals(port))
				destinations.add(channel.get_destination());
		}
		return destinations;
	}

	private void fillInRouting_H_String() {
		
		_routing_h_textToProduce += "#ifndef __GENERATED_ROUTING_H__\n";
		_routing_h_textToProduce += "#define __GENERATED_ROUTING_H__\n";
		
		PokModule module = PokModule.get();
		List<PokChannel> channels = module.get_channelList();
		int nbOfChannels = channels.size();
		if(nbOfChannels==0)
		{
			_routing_h_textToProduce += "#endif\n";
			return;
		}
			
		
		// TODO: add variability in number of nodes, thus making a difference
		// between local and global ports.
		
		List<PokPort> allPortsList = new ArrayList<PokPort>();
		for(PokPartition partition: module.get_partitionList())
		{
			List<PokPort> portList = partition.get_patritionPortList();
			allPortsList.addAll(portList);
		}
		_routing_h_textToProduce += "#define POK_CONFIG_NB_GLOBAL_PORTS "+allPortsList.size()+"\n";
		_routing_h_textToProduce += "#define POK_CONFIG_NB_PORTS "+allPortsList.size()+"\n";
		_routing_h_textToProduce += "#define POK_CONFIG_NB_NODES 1\n";
		
		
		_routing_h_textToProduce += "#define POK_CONFIG_PARTITIONS_PORTS {";
		for(PokPort port: allPortsList)
		{
			_routing_h_textToProduce += module.
					get_partitionList().
					indexOf(port.get_parent())+",";
		}
		_routing_h_textToProduce += "}\n";
		
		_routing_h_textToProduce += "typedef enum\n";
		_routing_h_textToProduce += "{\n";
		_routing_h_textToProduce += "  the_cpu = 0,\n";
		_routing_h_textToProduce += "} pok_node_identifier_t;\n";

		_routing_h_textToProduce += "typedef enum\n";
		_routing_h_textToProduce += "{\n";
		_routing_h_textToProduce += "  invalid_bus = 0\n";
		_routing_h_textToProduce += "} pok_bus_identifier_t;\n";
		
		_routing_h_textToProduce += "typedef enum\n";
		_routing_h_textToProduce += "{\n";
		int i = 0;
		for(PokPort port: allPortsList)
		{
			_routing_h_textToProduce += "  ";
			_routing_h_textToProduce += port.get_C_LocalIdentifier();
			_routing_h_textToProduce += " = ";
			_routing_h_textToProduce += i+",\n";
			i++;
		}
		_routing_h_textToProduce += "  invalid_local_port = "+i+"\n";
		_routing_h_textToProduce += "} pok_port_local_identifier_t;\n";
		
		// TODO: modify when adding possibility to define several nodes
		_routing_h_textToProduce += "typedef enum\n";
		_routing_h_textToProduce += "{\n";
		i=0;
		for(PokPort port: allPortsList)
		{
			_routing_h_textToProduce += "  ";
			_routing_h_textToProduce += port.get_C_GlobalIdentifier();
			_routing_h_textToProduce += " = ";
			_routing_h_textToProduce += i+",\n";
			i++;
		}
		_routing_h_textToProduce += "} pok_port_identifier_t;\n";
		_routing_h_textToProduce += "#endif\n";
	}

	private void fillInDeployment_C_String() {
		_deployment_c_textToProduce += "#include <types.h>\n";
		_deployment_c_textToProduce += "#include \"deployment.h\"\n";
		_deployment_c_textToProduce += "uint32_t pok_ports_names_max_len = 80;";
		// TODO: manage HM functions
//		void pok_kernel_error
//	    (uint32_t error)
//	{
//	  switch (error)
//	  {
//	    case POK_ERROR_KIND_NUMERIC_ERROR:
//	    {
//	      pok_kernel_stop();
//	      break;
//	    }
//	    case POK_ERROR_KIND_ILLEGAL_REQUEST:
//	    {
//	      pok_kernel_restart();
//	      break;
//	    }
//	  }
//	}
//	void pok_partition_error
//	    (uint8_t partition, uint32_t error)
//	{
//	  switch (partition)
//	  {
//	    case 0:
//	      switch (error)
//	      {
//	        case POK_ERROR_KIND_NUMERIC_ERROR:
//	        {
//	          pok_partition_set_mode(0, POK_PARTITION_MODE_RESTART);
//	          break;
//	        }
//	        case POK_ERROR_KIND_ILLEGAL_REQUEST:
//	        {
//	          pok_partition_set_mode(0, POK_PARTITION_MODE_RESTART);
//	          break;
//	        }
//	      }
//	    break;
//	    case 1:
//	      switch (error)
//	      {
//	        case POK_ERROR_KIND_NUMERIC_ERROR:
//	        {
//	          pok_partition_set_mode(1, POK_PARTITION_MODE_RESTART);
//	          break;
//	        }
//	        case POK_ERROR_KIND_ILLEGAL_REQUEST:
//	        {
//	          pok_partition_set_mode(1, POK_PARTITION_MODE_RESTART);
//	          break;
//	        }
//	      }
//	    break;
//	  }
//	}
	}
	
	private void fillInDeployment_H_String()
	{
		PokModule module = PokModule.get();
		_deployment_h_textToProduce += "#ifndef __GENERATED_DEPLOYMENT_H__\n";
		_deployment_h_textToProduce += "#define __GENERATED_DEPLOYMENT_H__\n";
		_deployment_h_textToProduce += "#include \"routing.h\"\n";
		_deployment_h_textToProduce += "#define POK_NEEDS_DEBUG 1\n";
		_deployment_h_textToProduce += "#define POK_NEEDS_CONSOLE 1\n";
		_deployment_h_textToProduce += "#define POK_CONFIG_LOCAL_NODE 0\n";
		_deployment_h_textToProduce += "#define POK_GENERATED_CODE 1\n";
		_deployment_h_textToProduce += "#define POK_NEEDS_GETTICK 1\n";
		_deployment_h_textToProduce += "#define POK_NEEDS_THREADS 1\n";
		_deployment_h_textToProduce += "#define POK_NEEDS_PARTITIONS 1\n";
		_deployment_h_textToProduce += "#define POK_NEEDS_USER_DEBUG 1\n";
		_deployment_h_textToProduce += "#define POK_NEEDS_SCHED 1\n";
		List<PokPartition> partitionList = module.get_partitionList(); 
		int nbOfPartitions = partitionList.size();
		if(nbOfPartitions==0)
		{
			System.err.println("Number of partitions: zero");
			System.exit(-1);
		}
		
		_deployment_h_textToProduce += "#define POK_CONFIG_NB_PARTITIONS "+nbOfPartitions+"\n";
		
		_deployment_h_textToProduce += "#define POK_CONFIG_NB_PROCESSORS 1\n";
		_deployment_h_textToProduce += "#define POK_CONFIG_PROCESSOR_AFFINITY {";
		for(PokPartition partition: partitionList)
		{
			_deployment_h_textToProduce +="1";
			if(!partition.equals(partitionList.get(partitionList.size()-1)))
				_deployment_h_textToProduce +=",";
		}
		_deployment_h_textToProduce +="}\n";
		int nbOfThreads = 0;
		int[] threadsPerPartition = new int[nbOfPartitions];
		for(PokPartition partition: partitionList)
		{
			int nbOfProcessesInPartition = partition.get_numberOfProcesses();
			int partitionId = partitionList.indexOf(partition);
			if(nbOfProcessesInPartition == 0)
			{
				System.err.println("Number of threads in partition "+partitionId+": zero");
				System.exit(-1);
			}
			nbOfThreads += nbOfProcessesInPartition;
			threadsPerPartition[partitionId] = nbOfProcessesInPartition+1;
		}
		nbOfThreads+=partitionList.size();
		nbOfThreads+=1; // 1 per core, monocore here
		_deployment_h_textToProduce += "#define POK_CONFIG_NB_THREADS "+nbOfThreads+"\n";
		_deployment_h_textToProduce += "#define POK_CONFIG_PARTITIONS_NTHREADS {"+threadsPerPartition[0];
		for(int i = 1; i<nbOfPartitions; i++)
		{
			_deployment_h_textToProduce += ","+threadsPerPartition[i];
		}
		_deployment_h_textToProduce +="}\n";
		
		_deployment_h_textToProduce += "#define POK_NEEDS_SCHED_RMS 1\n";
		
		// TODO : add variability in scheduler's selection
		_deployment_h_textToProduce += "#define POK_CONFIG_PARTITIONS_SCHEDULER {POK_SCHED_RMS";
		for(int i = 1; i<nbOfPartitions; i++)
		{
			_deployment_h_textToProduce += ",POK_SCHED_RMS";
		}
		_deployment_h_textToProduce += "}\n";
		
		List<PokPort> pokPortList = new ArrayList<PokPort>();
		for(PokPartition partition: partitionList)
			pokPortList.addAll(partition.get_patritionPortList());
		
		for(PokPort port: pokPortList)
			if(port.isSamplingPort())
			{
				_deployment_h_textToProduce += "#define POK_NEEDS_PORTS_SAMPLING 1\n";
				break;
			}
		
		for(PokPort port: pokPortList)
			if(port.isQueueingPort())
			{
				_deployment_h_textToProduce += "#define POK_NEEDS_PORTS_QUEUEING 1\n";
				break;
			}
		
		_deployment_h_textToProduce += "#define POK_NEEDS_THREAD_ID 1\n";
		
		if(!pokPortList.isEmpty())
		{
			_deployment_h_textToProduce += "#define POK_NEEDS_LOCKOBJECTS 1\n";

			// 1 per queueing port, 1 per sampling port
			_deployment_h_textToProduce += "#define POK_CONFIG_PARTITIONS_NLOCKOBJECTS {";
			for(PokPartition partition: partitionList) {
				_deployment_h_textToProduce += partition.get_patritionPortList().size();
				if(!partition.equals(partitionList.get(partitionList.size()-1)))
					_deployment_h_textToProduce+=",";
			}
			_deployment_h_textToProduce += "}\n";

			_deployment_h_textToProduce += "#define POK_CONFIG_NB_LOCKOBJECTS "+pokPortList.size()+"\n";
		}
		_deployment_h_textToProduce += "#define POK_CONFIG_PARTITIONS_SIZE {"+
				partitionList.get(0).get_partitionSize();
		int i=1;
		while(i<partitionList.size())
		{
			_deployment_h_textToProduce +=","+partitionList.get(i).get_partitionSize();
			i++;
		}
		_deployment_h_textToProduce += "}\n";
		
		List<PokPartitionWindow> windowList = module.get_partitionWindowList();
		_deployment_h_textToProduce += "#define POK_CONFIG_SCHEDULING_NBSLOTS "+ 
				windowList.size() +"\n";
		_deployment_h_textToProduce += "#define POK_CONFIG_SCHEDULING_SLOTS {"+
				windowList.get(0).get_duration();
		i=1;
		while(i<windowList.size())
		{
			_deployment_h_textToProduce +=","+windowList.get(i).get_duration();
			i++;
		}
		_deployment_h_textToProduce += "}\n";
		
		_deployment_h_textToProduce += "#define POK_CONFIG_SCHEDULING_SLOTS_ALLOCATION {"+
				partitionList.indexOf(windowList.get(0).get_partition());
		i=1;
		while(i<windowList.size())
		{
			_deployment_h_textToProduce +=","+
					partitionList.indexOf(windowList.get(i).get_partition());
			i++;
		}
		_deployment_h_textToProduce += "}\n";
		_deployment_h_textToProduce += "#define POK_CONFIG_SCHEDULING_MAJOR_FRAME "+module.get_majorFrame()+"\n";		

		if(module.get_flushType().equals(FlushType.MIF))
		{
			_deployment_h_textToProduce += "#define POK_FLUSH_PERIOD "+
					module.get_minorFrame();
			_deployment_h_textToProduce += "\n";
		}
		else if(module.get_flushType().equals(FlushType.WINDOW))
			_deployment_h_textToProduce += "#define POK_NEEDS_FLUSH_ON_WINDOWS\n";
		
		// TODO: add variability on stacks size
		_deployment_h_textToProduce += "#define POK_CONFIG_STACKS_SIZE 80000\n";
		
		_deployment_h_textToProduce += "#define POK_CONFIG_NB_BUSES 0\n";
		// TODO: include if error management OK
//		_deployment_h_textToProduce += "#define POK_NEEDS_ERROR_HANDLING 1\n";
//		_deployment_h_textToProduce += "#define POK_USE_GENERATED_KERNEL_ERROR_HANDLER 1\n";
//		_deployment_h_textToProduce += "#define POK_USE_GENERATED_PARTITION_ERROR_HANDLER 1\n";
		_deployment_h_textToProduce += "#include \"core/partition.h\"\n";
		_deployment_h_textToProduce += "#include \"core/error.h\"\n";
		_deployment_h_textToProduce += "#include \"core/kernel.h\"\n";
		_deployment_h_textToProduce += "#endif\n";
		
	}
	
	private void stringsToFiles(String outputDirPath) throws FileNotFoundException
	{
		
		String kernelDirName = "kernel/";
		if(outputDirPath.endsWith("/"))
			outputDirPath+=kernelDirName;
		else
			outputDirPath+="/"+kernelDirName;
		
		File outputDir = new File(outputDirPath);
		if(!outputDir.exists())
			outputDir.mkdir();
		
		PrintWriter deployment_h_printer = new PrintWriter(outputDirPath+"deployment.h");
		deployment_h_printer.println(_deployment_h_textToProduce);
		deployment_h_printer.close();
		
		PrintWriter deployment_c_printer = new PrintWriter(outputDirPath+"deployment.c");
		deployment_c_printer.println(_deployment_c_textToProduce);
		deployment_c_printer.close();
		
		PrintWriter routing_h_printer = new PrintWriter(outputDirPath+"routing.h");
		routing_h_printer.println(_routing_h_textToProduce);
		routing_h_printer.close();
		
		PrintWriter routing_c_printer = new PrintWriter(outputDirPath+"routing.c");
		routing_c_printer.println(_routing_c_textToProduce);
		routing_c_printer.close();
		
		PrintWriter makefile_printer = new PrintWriter(outputDirPath+"Makefile");
		makefile_printer.println(_makefile_textToProduce);
		makefile_printer.close();
	}
}
