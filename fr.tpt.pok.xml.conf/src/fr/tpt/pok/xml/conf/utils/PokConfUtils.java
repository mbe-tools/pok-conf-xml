package fr.tpt.pok.xml.conf.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.tpt.pok.xml.conf.model.PokModule;
import fr.tpt.pok.xml.conf.model.PokPartition;
import fr.tpt.pok.xml.conf.model.PokPort;

public class PokConfUtils {

	public static boolean containsSpecialCharacter(String s)
	{
		Pattern p = Pattern.compile("[^a-z0-9_ ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(s);
		return m.find();
	}

	public static String removeSpaces(String _name) {
		return _name.replace(' ', '_');
	}

	public static List<String> partitionsWithSameNames() {
		List<String> result = new ArrayList<String>();
		List<String> examinedNameList = new ArrayList<String>();
		for(PokPartition partition: 
			PokModule.get().get_partitionList())
		{
			String partitionName = partition.get_name();
			if(examinedNameList.contains(partitionName))
				result.add(partitionName);
			else
				examinedNameList.add(partitionName);
		}
		return result;
	}
	
	public static PokPartition getPartition(String searchedPartitionName)
	{
		for(PokPartition partition: 
			PokModule.get().get_partitionList())
		{
			String existingPartitionName = partition.get_name();
			if(existingPartitionName.equals(searchedPartitionName))
				return partition;
			
		}
		System.err.println("ERROR: partition "+
				searchedPartitionName+
				" referenced in XML file, but not defined");
		System.exit(-1);
		return null;
	}
	
	public static PokPort getPartitionPort(PokPartition partition, String searchedPortName)
	{
		for(PokPort port: 
			partition.get_patritionPortList())
		{
			String existingPortName = port.get_name();
			if(existingPortName.equals(searchedPortName))
				return port;
			
		}
		System.err.println("ERROR: port "+
				searchedPortName+
				" referenced in XML file, is not defined in partition "+
				partition.get_name());
		System.exit(-1);
		return null;
	}
	
}
