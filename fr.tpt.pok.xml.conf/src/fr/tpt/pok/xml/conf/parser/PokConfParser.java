package fr.tpt.pok.xml.conf.parser;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import fr.tpt.pok.xml.conf.model.PokChannel;
import fr.tpt.pok.xml.conf.model.PokModule;
import fr.tpt.pok.xml.conf.model.PokPartition;
import fr.tpt.pok.xml.conf.model.PokPartitionWindow;
import fr.tpt.pok.xml.conf.model.PokPort;
import fr.tpt.pok.xml.conf.utils.PokConfUtils;

public class PokConfParser {

	public void parseConfigurationFile(String xmlFilePath)
	{
		
		SAXBuilder sxb = new SAXBuilder();
		Document document = null;
		
		try {
			document = sxb.build(new File(xmlFilePath));
		} catch (JDOMException e) {
			System.err.println("JDOMException caught, your XML "
					+ "configuration file is probably ill-formed "
					+ "(see stack trace bellow for more information)");
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	
		PokModule module = PokModule.get();
		
		Element root = document.getRootElement();
		
		Attribute majorFrameAttr = root.getAttribute("major_frame");
		if(majorFrameAttr==null)
		{
			System.err.println("ERROR: major frame attribute undefined");
			System.exit(-1);
		}
		String majorFrameString = majorFrameAttr.getValue();
		Integer majorFrame = Integer.parseInt(majorFrameString);
		module.set_majorFrame(majorFrame);
		
		Attribute minorFrameAttr = root.getAttribute("minor_frame");
		if(minorFrameAttr!=null)
		{
			String minorFrameString = minorFrameAttr.getValue();
			Integer minorFrame = Integer.parseInt(minorFrameString);
			module.set_minorFrame(minorFrame);
		}
		
		Attribute flushTypeAttr = root.getAttribute("flush_type");
		if(flushTypeAttr!=null)
		{
			String flushTypeString = flushTypeAttr.getValue();
			module.set_flushType(flushTypeString);
		}
		
		List<Element> partitionElementList = root.getChildren("Partition");
		
		Iterator<Element> partitionIterator = partitionElementList.iterator();
		while(partitionIterator.hasNext())
		{
			Element partitionElement = (Element) partitionIterator.next();
			if(partitionElement==null)
			{
				System.err.println("No partition found");
				System.exit(-1);
			}
			Attribute nameElement = partitionElement.getAttribute("name");
			if(nameElement==null)
			{
				System.err.println("ERROR: Partition name undefined");
				System.exit(-1);
			}
			String partitionName = nameElement.getValue();
			if(partitionName==null || partitionName.isEmpty())
			{
				System.err.println("ERROR: Partition name undefined");
				System.exit(-1);
			}
			Attribute partitionSizeElement = partitionElement.getAttribute("partition_size");
			if(partitionSizeElement==null)
			{
				System.err.println("ERROR: undefined partition size for partition "+partitionName);
				System.exit(-1);
			}
			String partitionSizeString = partitionSizeElement.getValue();
			Integer partitionSize = Integer.parseInt(partitionSizeString);
			if(partitionSize<=0)
			{
				System.err.println("ERROR: undefined partition size for partition "+partitionName);
				System.exit(-1);
			}
			Attribute numberOfProcessesElement = partitionElement.getAttribute("number_of_processes");
			if(numberOfProcessesElement==null)
			{
				System.err.println("ERROR: undefined number of processes for partition "+partitionName);
				System.exit(-1);
			}
			String numberOfProcessesString = numberOfProcessesElement.getValue();
			Integer numberOfProcesses = Integer.parseInt(numberOfProcessesString);
			if(numberOfProcesses<=0)
			{
				System.err.println("ERROR: undefined number of processes for partition "+partitionName);
				System.exit(-1);
			}
			PokPartition partition = new PokPartition(partitionName, 
					partitionSize, 
					numberOfProcesses);
			
			List<Element> portElementList = partitionElement.getChildren("Port");
			
			Iterator<Element> portIterator = portElementList.iterator();
			while(portIterator.hasNext())
			{
				Element portElement = (Element) portIterator.next();
				Attribute portNameElement = portElement.getAttribute("name"); 
				if(portNameElement==null)
				{
					System.err.println("ERROR: undefined name for a port in partition "+partitionName);
					System.exit(-1);
				}
				String portName = portNameElement.getValue();
				if(portName==null || portName.isEmpty())
				{
					System.err.println("ERROR: undefined name for a port in partition "+partitionName);
					System.exit(-1);
				}
				
				Attribute portKindElement = portElement.getAttribute("kind"); 
				if(portKindElement==null)
				{
					System.err.println("ERROR: undefined kind (samplig|queueing) for a port in partition "+partitionName);
					System.exit(-1);
				}
				String portKind = portKindElement.getValue();
				if(portKind==null || portKind.isEmpty())
				{
					System.err.println("ERROR: undefined kind (samplig|queueing) for a port in partition "+partitionName);
					System.exit(-1);
				}
				
				Attribute portDirectionElement = portElement.getAttribute("direction"); 
				if(portDirectionElement==null)
				{
					System.err.println("ERROR: Undefined direction (in|out) for a port in partition "+partitionName);
					System.exit(-1);
				}
				String portDirection = portDirectionElement.getValue();
				if(portDirection==null || portDirection.isEmpty())
				{
					System.err.println("ERROR: Undefined direction (in|out) for a port in partition "+partitionName);
					System.exit(-1);
				}
				
				PokPort port = new PokPort(portName, partition, portKind, portDirection);
				partition.add_PartitionPort(port);
			}
			
			module.get_partitionList().add(partition);
		}
		
		
		List<Element> channelElementList = root.getChildren("Channel");
		Iterator<Element> channelIterator = channelElementList.iterator();
		
		while(channelIterator.hasNext())
		{
			Element channelElement = (Element) channelIterator.next();
			
			// source
			Element sourceElement = channelElement.getChild("Source");
			if(sourceElement==null)
			{
				System.err.println("ERROR: Channel element is missing a <Source/> element");
				System.exit(-1);
			}
			Attribute sourcePartitionAttr = sourceElement.getAttribute("partition_name");
			if(sourcePartitionAttr==null)
			{
				System.err.println("ERROR: Source element in a channel "
						+ "is missing a partition_name attribute");
				System.exit(-1);
			}
			String sourcePartitionName = sourcePartitionAttr.getValue();
			if(sourcePartitionName==null||sourcePartitionName.isEmpty())
			{
				System.err.println("ERROR: partition_name attribute is null "
						+ "or empty in a source of a channel");
				System.exit(-1);
			}
			PokPartition sourcePartition = PokConfUtils.getPartition(sourcePartitionName);
			Attribute sourcePortAttr = sourceElement.getAttribute("port_name");
			if(sourcePortAttr==null)
			{
				System.err.println("ERROR: Source element in a channel "
						+ "is missing a port_name attribute");
				System.exit(-1);
			}
			String sourcePortName = sourcePortAttr.getValue();
			if(sourcePortName==null||sourcePortName.isEmpty())
			{
				System.err.println("ERROR: port_name attribute is null "
						+ "or empty in a source of a channel");
				System.exit(-1);
			}
			PokPort sourcePort = PokConfUtils.
					getPartitionPort(sourcePartition, sourcePortName);
			// TODO make sure source port is an output port
			
			//destination
			Element destinationElement = channelElement.getChild("Destination");
			if(destinationElement==null)
			{
				System.err.println("ERROR: Channel element is missing a <Destination/> element");
				System.exit(-1);
			}
			Attribute destinationPartitionAttr = destinationElement.getAttribute("partition_name");
			if(destinationPartitionAttr==null)
			{
				System.err.println("ERROR: Destination element in a channel "
						+ "is missing a partition_name attribute");
				System.exit(-1);
			}
			String destinationPartitionName = destinationPartitionAttr.getValue();
			if(destinationPartitionName==null||destinationPartitionName.isEmpty())
			{
				System.err.println("ERROR: partition_name attribute is null "
						+ "or empty in a destination of a channel");
				System.exit(-1);
			}
			PokPartition destinationPartition = PokConfUtils.
					getPartition(destinationPartitionName);
			Attribute destinationPortAttr = destinationElement.getAttribute("port_name");
			if(destinationPortAttr==null)
			{
				System.err.println("ERROR: Destination element in a channel "
						+ "is missing a port_name attribute");
				System.exit(-1);
			}
			String destinationPortName = destinationPortAttr.getValue();
			if(destinationPortName==null||destinationPortName.isEmpty())
			{
				System.err.println("ERROR: port_name attribute is null "
						+ "or empty in a destination of a channel");
				System.exit(-1);
			}
			PokPort destinationPort = PokConfUtils.
					getPartitionPort(destinationPartition, destinationPortName);
			
			// TODO make sure destination port is an input port
			
			PokChannel channel = new PokChannel(sourcePort, destinationPort);
			PokModule.get().get_channelList().add(channel);
		}
		
		List<Element> partitionWindowElementList = root.getChildren("PartitionWindow");
		if(partitionWindowElementList == null ||
				partitionWindowElementList.isEmpty())
		{
			System.err.println("ERROR: PartitionWindow element "
					+ "undefined");
			System.exit(-1);
		}
		Iterator<Element> partitionWindowIterator = partitionWindowElementList.iterator();
		
		while(partitionWindowIterator.hasNext())
		{
			Element partitionWindowElement = (Element) partitionWindowIterator.next();
			
			//partition
			Attribute windowPartitionAttr = partitionWindowElement.getAttribute("partition_name");
			if(windowPartitionAttr==null)
			{
				System.err.println("ERROR: PartitionWindow element "
						+ "is missing a partition_name attribute");
				System.exit(-1);
			}
			String partitionName = windowPartitionAttr.getValue();
			PokPartition partition = PokConfUtils.
					getPartition(partitionName);
			//duration
			Attribute windowDurationAttr = partitionWindowElement.getAttribute("duration");
			if(windowDurationAttr==null)
			{
				System.err.println("ERROR: PartitionWindow element "
						+ "is missing a duration attribute");
				System.exit(-1);
			}
			String durationString = windowDurationAttr.getValue();
			int duration = Integer.parseInt(durationString);
			
			PokPartitionWindow window = new PokPartitionWindow(partition, duration);
			PokModule.get().get_partitionWindowList().add(window);
			
		}
			
		
	}
	
}
