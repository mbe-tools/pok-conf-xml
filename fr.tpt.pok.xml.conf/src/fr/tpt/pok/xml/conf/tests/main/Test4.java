package fr.tpt.pok.xml.conf.tests.main;

import org.junit.Test;

public class Test4 extends TestScenario {

	@Override
	protected String get_inputFilePath() {
		return "tests/test4/input/example4.xml";
	}

	@Override
	protected String get_outputFilePath() {
		return "tests/test4/output/";
	}

	@Test
	public void test() {
		super.test();
	}
	
}
