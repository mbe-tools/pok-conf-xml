package fr.tpt.pok.xml.conf.tests.main;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.jdom2.Document;

import fr.tpt.pok.xml.conf.backend.PokMainConfigurationGenerator;
import fr.tpt.pok.xml.conf.parser.PokConfParser;

public abstract class TestScenario {

	protected abstract String get_inputFilePath();
	
	protected abstract String get_outputFilePath();
	
	public void test() {
		PokConfParser parser = new PokConfParser();
		PokMainConfigurationGenerator gen = new PokMainConfigurationGenerator();
		String file_path1 = get_inputFilePath();
		String file_path2 = get_outputFilePath();
		parser.parseConfigurationFile(file_path1);
		gen.generateFiles(file_path2);
		assertTrue(true);
	}

	protected StringBuilder getContent(String filePath) throws IOException
	{
		String thisLine;
		FileInputStream fin =  new FileInputStream(filePath);
		BufferedReader myInput = new BufferedReader(new InputStreamReader(fin));
		StringBuilder sb = new StringBuilder();
		while ((thisLine = myInput.readLine()) != null) {  
			sb.append(thisLine);
		}
		myInput.close();
		return sb;
	}
	
	protected Document getParsingResult()
	{
		// PokModule module = PokModule.get();
		
		// add major/minor_frame
		// add Partitions
		// add Ports
		// add Channels
		// ...
		return null;
	}
}
