package fr.tpt.pok.xml.conf.tests.main;

import org.junit.Test;

public class Test1 extends TestScenario {

	@Override
	protected String get_inputFilePath() {
		return "tests/test1/input/example1.xml";
	}

	@Override
	protected String get_outputFilePath() {
		return "tests/test1/output/";
	}
	
	@Test
	public void test() {
		super.test();
	}
	
}
