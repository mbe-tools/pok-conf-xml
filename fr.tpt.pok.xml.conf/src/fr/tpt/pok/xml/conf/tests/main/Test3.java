package fr.tpt.pok.xml.conf.tests.main;

import org.junit.Test;

public class Test3 extends TestScenario {

	@Override
	protected String get_inputFilePath() {
		return "tests/test3/input/example3.xml";
	}

	@Override
	protected String get_outputFilePath() {
		return "tests/test3/output/";
	}

	@Test
	public void test() {
		super.test();
	}
	
}
