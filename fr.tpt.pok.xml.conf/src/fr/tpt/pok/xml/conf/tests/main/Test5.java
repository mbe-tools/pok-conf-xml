package fr.tpt.pok.xml.conf.tests.main;

import org.junit.Test;

public class Test5 extends TestScenario {

	@Override
	protected String get_inputFilePath() {
		return "tests/test5/input/example5.xml";
	}

	@Override
	protected String get_outputFilePath() {
		return "tests/test5/output/";
	}

	@Test
	public void test() {
		super.test();
	}
	
}
