package fr.tpt.pok.xml.conf.tests.main;

import org.junit.Test;

public class Test2 extends TestScenario {

	@Override
	protected String get_inputFilePath() {
		return "tests/test2/input/example2.xml";
	}

	@Override
	protected String get_outputFilePath() {
		return "tests/test2/output/";
	}

	@Test
	public void test() {
		super.test();
	}
	
}
